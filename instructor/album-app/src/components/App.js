import React, { useState, useEffect } from "react";
import axios from "axios";

import "./App.css";
import Banner from "./Banner";
import Album from "./Album";

const App = () => {
	const [albums, setAlbums] = useState([]);

	// same as componentDidMount in Class Component
	useEffect(() => {
		console.log("App is mounted");
		axios.get("http://localhost:8088/albums").then((res) => {
			console.log(res);
			setAlbums(res.data);
		});
	}, []); // if there is nothing in the array, it will only run once

	return (
		<div className="app">
			<Banner />
			<div className="container">
				<div className="row">
					{albums.map((album) => (
						<Album
							key={album.title}
							title={album.title}
							artist={album.artist}
							tracks={album.tracks}
						/>
					))}
				</div>
			</div>
		</div>
	);
};

export default App;

// const albums = [
// 	{
// 		title: "Album 1",
// 		artist: "Artist 1",
// 		tracks: ["Track 1", "Track 2", "Track 3"],
// 	},
// 	{
// 		title: "Album 2",
// 		artist: "Artist 2",
// 		tracks: ["Track 4", "Track 5", "Track 6"],
// 	},
// 	{
// 		title: "Album 3",
// 		artist: "Artist 3",
// 		tracks: ["Track 6", "Track 7", "Track 9"],
// 	},
// 	{
// 		title: "Album 4",
// 		artist: "Artist 4",
// 		tracks: ["Track 1", "Track 2", "Track 3"],
// 	},
// 	{
// 		title: "Album 5",
// 		artist: "Artist 5",
// 		tracks: ["Track 4", "Track 5", "Track 6"],
// 	},
// 	{
// 		title: "Album 6",
// 		artist: "Artist 6",
// 		tracks: ["Track 6", "Track 7", "Track 9"],
// 	},
// ];

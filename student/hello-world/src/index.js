import React from "react";
import ReactDOM from "react-dom";

import {} from "./styles/index.css";

// const header = React.createElement("h1", null, "My header");

// const text = React.createElement("p",
// {className:"hello-text", id:"hello-id"}
// , "Hello World!");

// const main =  React.createElement(
//     "div",
//     {className: "hello-container"},
//     header,
//     text
// );

// //using Create Element
// const main = React.createElement( "div", { id: "ice-cream" }, [
//     React.createElement( "h1", null, "Ice Cream Flavours" ),
//     React.createElement( "ul", { class: "ice-cream-list" }, [
//         React.createElement( "li", null, "Vanilla" ),
//         React.createElement( "li", null, "Chocolate" ),
//         React.createElement( "li", null, "Raspberry Ripple" )
//     ] )
// ] );

//Using JSC
function getFlavors() {
	return (
		<React.Fragment>
			<h2>Ice Cream Flavours</h2>
			<ul class="ice-cream-list">
				<li>Vanilla</li>
				<li className="choco">Chocolate</li>
				<li className="rasp-color">Raspberry Ripple</li>
			</ul>
		</React.Fragment>
	);
}

const shopName = "Var Ice Cream shop";
const main = (
	<section data-shop="Allstate">
		<div>
			<div>
				<h1>{shopName}</h1>
			</div>
			<div id="ice-cream">{getFlavors()}</div>
		</div>
	</section>
);

ReactDOM.render(main, document.getElementById("root"));

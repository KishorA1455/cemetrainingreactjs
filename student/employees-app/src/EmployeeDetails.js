import React from "react";

const EmployeeDetails = (props) => {
	return (
		props.visible && (
			<div>
				<div>{props.id}</div>
				<div>{props.age}</div>
				<div>{props.email}</div>
				<div>{props.salary}</div>
			</div>
		)
	);
};

export default EmployeeDetails;

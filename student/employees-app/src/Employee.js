import React, { useState } from "react";
import { render } from "react-dom";
import EmployeeDetails from "./EmployeeDetails";
import Toggle from "./ToggleButton";

const Employee = (props) => {
	const [visible, setVisibility] = useState(true);
	return (
		<div className="card md-4 box-shadow" style={{ width: "18rem" }}>
			<div className="card-body">
				<EmployeeDetails
					id={props.id}
					age={props.age}
					email={props.email}
					salary={props.salary}
					visible={visible}
				/>
				<Toggle clicked={setVisibility} visible={visible} />
			</div>
		</div>
	);
};

export default Employee;

import React from "react";

const Toggle = (props) => {
	return (
		<button
			className="btn btn-sm btn-outline-secondary"
			onClick={() => props.clicked(!props.visible)}
		>
			Toggle
		</button>
	);
};

export default Toggle;

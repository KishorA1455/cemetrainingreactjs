import React from "react";
import ReactDOM from "react-dom";
import Employee from "./Employee";
import EmployeeList from "./EmployeeList";

ReactDOM.render(
	<div>
		<EmployeeList />
	</div>,
	document.querySelector("#root")
);

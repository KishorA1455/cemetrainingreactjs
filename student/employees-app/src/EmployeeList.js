import React from "react";
import Employee from "./Employee";
// import "./App.css";

const EmployeeList = () => {
	const employees = [
		{
			id: 1,
			name: "Rocky",
			age: 30,
			email: "rambostar@gmail.com",
			salary: 100,
		},
		{
			id: 1,
			name: "John",
			age: 32,
			email: "johnwest@gmail.com",
			salary: 99,
		},
		{
			id: 1,
			name: "Ram",
			age: 25,
			email: "ramtruck22@gmail.com",
			salary: 110,
		},
	];

	return employees.map((e) => (
		<div className="app container">
			<div className="row">
				<div className="col-md-4">
					<h3>{e.name}</h3>
					<Employee id={e.id} age={e.age} email={e.email} salary={e.salary} />
				</div>
			</div>
		</div>
	));
};

export default EmployeeList;

import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App";

ReactDOM.render(
	<div>
		<h2>Album app</h2>
		<App />
	</div>,
	document.getElementById("root")
);

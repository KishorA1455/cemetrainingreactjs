import React, { useState, useEffect } from "react";
import axios from "axios";
import "./../styles/style.css";
import Album from "./Album";

const App = () => {
	const [albums, setAlbums] = useState([]);

	useEffect(() => {
		console.log("App is mounted");
		axios.get("http://localhost:8088/albums").then((res) => {
			console.log(res);
			setAlbums(res.data);
		});
	}, []);

	return (
		<div className="app">
			<div className="albums">
				{albums.map((album) => (
					<Album
						key={album.title}
						title={album.title}
						artist={album.artist}
						tracks={album.tracks}
					/>
				))}
			</div>
		</div>
	);
};

export default App;

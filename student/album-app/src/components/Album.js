import React, { useState } from "react";
import AlbumTitle from "./AlbumTitle";
import AlbumArtist from "./AlbumArtist";
import AlbumTracks from "./AlbumTracks";
import ShowHideButton from "./ShowHideButton";

const Album = (props) => {
	const [visible, setVisibility] = useState(true);

	return (
		<section>
			<AlbumTitle title={props.title} />
			<AlbumArtist artist={props.artist} />
			<ShowHideButton toggle={setVisibility} visible={visible} />
			<AlbumTracks tracks={props.tracks} visible={visible} />
		</section>
	);
};

export default Album;

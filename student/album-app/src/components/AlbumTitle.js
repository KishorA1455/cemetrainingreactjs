import React from "react";

const AlbumTitle = (props) => {
	return <h2 className="album-title">{props.title}</h2>;
};

export default AlbumTitle;

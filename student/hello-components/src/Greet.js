import React from "react";

// Class component
// class Greet extends React.Component {
// 	// constructor(props) {
// 	// 	super(props);
// 	// 	console.log(props);
// 	// }

// 	render() {
// 		return (
// 			<h1>
// 				Class component - Welcome {this.props.person} from {this.props.name}
// 			</h1>
// 		);
// 	}
// }

//this is a functional component that receives properties as props
const Greet = ({ name, person, height }) => (
	<h3>
		Hello {person} from {name}!!!
		{height ? `My height is ${height}.` : null}
	</h3>
);

export default Greet;

// function Greet(props) {
// 	// {name: "Allstate", person: "John"}
// 	//console.log(props);
// 	return (
// 		<h1>
// 			Hello {props.person} from {props.name}!
// 		</h1>
// 	);
// }

// function Greet({ name, person }) {
// 	// {name: "Allstate", person: "John"}
// 	//console.log(props);
// 	return (
// 		<h1>
// 			Hello {person} from {name}!
// 		</h1>
// 	);
// }

// const Greet = ({ name, person }) => {
// 	return (
// 		<h1>
// 			Arrow Function Example - Hello {person} from {name}!
// 		</h1>
// 	);
// };
